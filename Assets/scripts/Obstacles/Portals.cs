﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portals : MonoBehaviour, IObstacle {

    public GameObject flying_object;
    
    public Transform exit;

    public float inrange;
    //layer player
    public LayerMask layers_to_teleport = 8;
    public float radius = 0.01f;

	public IObstacle Obstacle() {
		return this;
    }

    // Use this for initialization
    void Start () {
        inrange = Random.Range(1, 5);
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        
        //teleport objects that intersect with with radius 
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layers_to_teleport);
        foreach (var collider in colliders)
        {
            
            collider.gameObject.transform.position = exit.position;
        }
    }
    //Instantiate exit gate in a range to the entry gate. Friendly or hostile?
    void InstantiateExit()
    {

    }


    
}
