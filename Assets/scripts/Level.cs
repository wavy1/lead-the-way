﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Level : MonoBehaviour {

	public Guid id;											// An Unique Id for every Level
	private int difficulty;									// Difficulty of level 
	public int Difficulty {
		get{ return difficulty;}
	}
	private Bounds bound;
	public Bounds Bound {
		get{ return bound; }
	}

	private Vector3 goal; 									// Where the flying Object has to get to in for the playBoard to play the next level
	public Vector3 Goal {
		set{goal = value;}
		get{return goal;}
	}

	private Dictionary<string, Vector3> obstacles;
	public Dictionary<string, Vector3> Obstacles {
		set{obstacles = value;}
		get { return obstacles; }
	}

	// Will construct a level and into the playBoard and flying Object, meaning Goals and Obstacles
	public Level(PlayingBoard playBoard, int levelNumber , Transform flyingObject){
		difficulty = levelNumber + 1;
		bound = new Bounds (new Vector3 (0f, 2.5f, 0f), new Vector3 (10f, 5f, 10f)); 		// Hard coded
		goal = playBoard.genStrat.CalculateGoalForLevel (this, flyingObject);
		obstacles = playBoard.genStrat.CalculateObstaclesForLevel (this);
		id = System.Guid.NewGuid ();
	}

	public override string ToString ()
	{
		string obstaclesStr = "";
		foreach (KeyValuePair<string, Vector3> obstacle in obstacles) {
			obstaclesStr += obstacle + " " + obstacle.Value + "\n";
		}
		return string.Format ("The Level {0} has the difficulty: {1},\n" +
								" the goal is on position {2},\n " +
									"the bounds are: {3}" +
										"the obstacles are : {4}"
											, id, difficulty, goal, bound, obstaclesStr);
	}
}
