﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface LevelGenerationStrategy {

	Vector3 CalculateGoalForLevel (Level level, Transform shipPosition);
	Dictionary<string, Vector3> CalculateObstaclesForLevel (Level level);
}
