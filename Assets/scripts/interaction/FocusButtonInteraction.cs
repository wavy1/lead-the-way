﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI; //testing purpose

public class FocusButtonInteraction : MonoBehaviour, ViewInteract {

	// Threshold for action to occur
	public float thresholdForInteraction = 3;
	public ButtonInteractionOutcome outcome;
	public GameObject helperPrefab;
    public Text progressText;

	private GameObject trackableOrigin;
	private GameObject flyingObject;
	private GameObject directionArrow;
    private GameObject shipInstance;

    public PlayingBoard playingBoard;
	private bool hasInteractionOccurred;

	void Awake(){

		trackableOrigin = GetComponentInParent<TrackableBehaviour> ().gameObject ?  null : trackableOrigin;
		GameObject playingBoardcanvasObj = gameObject.transform.parent.gameObject;
		playingBoard = GameObject.Find ("playboard").GetComponent<PlayingBoard> ();
		flyingObject = GameObject.Find ("ship_imageTarget");
		directionArrow = GameObject.Find ("directionArrow_imageTarget");
	}

	// Implementation of TimedInteract to start the game after the in this component defined threshold 
	public bool TimedInteract(float lookAtTime, RaycastHit hit){
		if (lookAtTime % 1 < 0.005f) Debug.Log ("Interaction with " + hit.collider.gameObject.name + lookAtTime +
			"Interaction possible: " + !hasInteractionOccurred);
		if (lookAtTime >= thresholdForInteraction && !hasInteractionOccurred) {
			hasInteractionOccurred = true;
			switch (outcome) {
			case ButtonInteractionOutcome.START:
				GameControl.instance.StartGame (playingBoard);
			    progressText.text = "Place Ship";
			    Destroy (this);
				Debug.Log ("Started Game");
				break;

			case (ButtonInteractionOutcome.GENERATE_GOAL):
				if (playingBoard.gameObject.activeSelf) playingBoard.ReInstGoal ();
				break;

			case ButtonInteractionOutcome.GENERATE_OBSTACLES:
				if (playingBoard.gameObject.activeSelf) playingBoard.ReInstObstacles ();
				break;

			case ButtonInteractionOutcome.REACHED_DESTINATION:
				Debug.Log ("Reached Destination");
				Debug.Log ("Next Level");
				if (playingBoard.gameObject.activeSelf)
					playingBoard.NextLevel ();
				break;

			case ButtonInteractionOutcome.PLACE_SHIP:
				Debug.Log ("Locked in ship");
				ARGameObjectControl.instance.PlaceOnPlayBoard (flyingObject.transform, playingBoard);
			    progressText.text = "Place DirectionArrow";
				GameControl.instance.StartGame (playingBoard);
                playingBoard.gameObject.transform.Find("start_ship_button").gameObject.SetActive(true);
			    break;

			case ButtonInteractionOutcome.PLACE_ARROW:
				Debug.Log ("Locked in Direction Arrow");
				ARGameObjectControl.instance.PlaceOnPlayBoard(directionArrow.transform, playingBoard, helperPrefab);
                break;

			case ButtonInteractionOutcome.TAKE_BACK:
				Debug.Log ("Takeing back object from playspace");
				ImageTargetBehaviour trackable = GameObject.Find ("" + hit.collider.name.Split ('_') [0] + "_imageTarget").GetComponent<ImageTargetBehaviour> ();
				Debug.Log (trackableOrigin);
				ARGameObjectControl.instance.unPlaceOnPlayBoard (hit.collider.gameObject, trackable.transform, helperPrefab);
				break;

            case ButtonInteractionOutcome.START_SHIP:
                Debug.Log("Take off fo");
                shipInstance = hit.collider.transform.parent.Find("ship_model").gameObject;
                shipInstance.GetComponentInChildren<ParticleSystem>().Play();
                shipInstance.GetComponent<ShipBehaviour>().TakeOff(20);
                
                break;
            default:
			    Debug.Log ("Default Interaction");
			    break;

			}
			Camera.main.GetComponent<RayView>().resetTimer();
			return true;
		} 
		return false;
	}

	public void Reset(){
		hasInteractionOccurred = false;
		Debug.Log ("Resetted Interact");
	}
}
